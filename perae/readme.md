# Desenvolvimento
## git clone <repo>
## npm install
## ionic serve -t android

# Build (Android)
## ionic platform add android
## ionic build android

# Inclusão de novo Audio
## Inserir arquivo mp3 em assets/audio/
## Incluir novo objeto Audio na lista presente no construtor da página HomePage.
## O construtor de Áudio recebe o título de identificação e o nome do arquivo mp3.

# Alteração de Audio existente
## Inserir arquivo mp3 na pasta assets/audio/
## Escrever o nome do novo arquivo mp3 no segundo parâmetro do construtor do objeto Audio.
