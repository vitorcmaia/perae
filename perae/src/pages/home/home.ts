import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Audio } from '../../model/audio';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

	audios: Array<Audio>;
	icon: string;

	constructor(public navCtrl: NavController) {
        this.icon = 'play';
        this.audios = [
            new Audio('Peraê', '01'),
            new Audio('Ei', '02'),
            new Audio('Não Não Não', '03'),
            new Audio('Huhuhuhu', '04'),
            new Audio('Valeu. Peraê', '05'),
            new Audio('Seu fia da mãe', '06'),
            new Audio('Menos eu', '07')
        ];
    }
}
