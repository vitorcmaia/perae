// Dados referente a um áudio.
export class Audio {

	name: string;
	src: string;
	art: string;

	constructor(name: string, fileName: string) {
		this.name = name;
		this.src = 'assets/audio/' + fileName + '.mp3';
		this.art = 'assets/general/prill.png'
	}

}
